import React, {useEffect, useState} from 'react';
import Tile from "../Tile/Tile";
import './Game.css'

Game.propTypes = {};


function generateRandomGrid() {
  return Array.from({length: 16}, (_, i) => i).sort(() => Math.random() - 0.5);
}

function Game({props}) {
  const [grid, setGrid] = useState([])

  useEffect(() => {
    setGrid(() => generateRandomGrid())
  }, [])

  const onTileClicked = (tileNumber) => {
    let zeroIndex = grid.indexOf(0);
    let tileIndex = grid.indexOf(tileNumber);

    if (tileIndex + 4 === zeroIndex || tileIndex - 4 === zeroIndex) {
      swapTiles(tileIndex, zeroIndex);
    } else if (tileIndex + 1 === zeroIndex && zeroIndex % 4 !== 0) {
      swapTiles(tileIndex, zeroIndex);
    } else if (tileIndex - 1 === zeroIndex && (zeroIndex + 1) % 4 !== 0) {
      swapTiles(tileIndex, zeroIndex)
    }
  }

  const swapTiles = (tileToSwapIndex, tileZeroIndex) => {
    let temArray = [...grid]
    temArray[tileZeroIndex] = grid[tileToSwapIndex];
    temArray[tileToSwapIndex] = 0;
    setGrid(() => [...temArray])
  }

  return (
    <div className="game-grid">
      {grid.map(gridItem => {
        return (
          <Tile key={gridItem} onClick={onTileClicked} number={gridItem}/>
        )
      })}
    </div>
  );
}

export default Game;
