import React from 'react';
import PropTypes from 'prop-types';
import './Tile.css';

Tile.propTypes = {
  number: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
};

function Tile({number, onClick}) {
  return (
    <div onClick={() => onClick(number)} className={`tile ${number === 0 ? 'empty' : ''}`}>
      {number}
    </div>
  );
}

export default Tile;
